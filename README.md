# Beat
========================

[![Platform](http://img.shields.io/badge/platform-ios-blue.svg?style=flat
)](https://developer.apple.com/iphone/index.action)
[![Language](http://img.shields.io/badge/language-swift-brightgreen.svg?style=flat
)](https://developer.apple.com/swift)
[![License](http://img.shields.io/badge/license-MIT-lightgrey.svg?style=flat
)](http://mit-license.org)

## Overview
iOS Google Maps client that display places around your current location on the map. Developed using Swift, Objective-C, VIPER and TDD.  


![search](Screenshots/Screenshot-search.png)
![map](Screenshots/Screenshot-map.png)
![map-list](Screenshots/Screenshot-map-list.png)
![list](Screenshots/Screenshot-list.png)


## App Description

The goal of this Objective-C/Swift application is to gather a list of Google places that are near to the current location and address, and display them on a tableview/map.

**App use cases**

*Search nearby places*:

- The first screen displays a textfield, which the user can use to write free text. When the user presses the search keyboard button, a search request is sent to the Google Places API (query the Api for candy stores or if you are not into candies for any other specific category that you may like), using as a location the current location of the user. This current location is obtained via the device's GPS, only once on app startup. After the places are obtained, the app automatically transfers the user to the second screen, where all places are displayed in a single UITableView and on a map.

*Map*:

- The second view is a combination of a tableView and a map. The map must be a Google map. The tableView displays all the places that are fetched form the previous call to the Google Places API, each cell containing the place’s relevant information. When the view is first displayed, the first row of the tableView is selected and the map displays the position (latitude and longitude) of the first place. When another row is selected, the map displays the location of the new selected place. If the user scrolls on the map and after the map is being idle at its new location, the app must search all the places that are displayed in the tableview and locate the map to the nearest place (nearest to the last position of the map where the user scrolled).

**Features**

* UI: the app is heavily inspired by iOS App Maps app. 
* Request retrier: if a request fail due to server error the app will retry that request again for 3 times each 1 second. Thanks to Alamofire's RequestRetrier, please check NetworkRequestRetrier. 

* In App Notification: if the device was offline and the app faild to make a request it will show in-app message to warn the user that there is not internet connection.

**Improvements**

* Increase unit test code coverage
* Could use reactive programming like `RxSwift` instead of delegation 
* optimize map screen to work more smoothly with LOTS OF annotations.

## Installation

Just clone the repo or download it in zip-file, Open the project in Xcode then test it on your iOS device or iOS simulator.


# Xcode Project files structure
```bash
.swift
+-- Common
|   +-- Extensions
    |   +-- UIColor+Palette.swift
|   +-- Components
    |   +-- Roller
+-- Storyboards
|   +-- NearbyPlaces.storyboard
|   +-- SearchPlaces.storyboard
+-- Models
|   +-- Place.swift
|   +-- SeachPlaceContent.swift
+-- Assets
|   +-- Assets.xcassets
+-- Base
|   +-- Network
+-- Modules
|   +-- Search
    |   +-- Interactor
        |   +-- SearchPlacesInteractable.h
        |   +-- SearchPlacesInteractor.h
		|   +-- SearchPlacesInteractor.m
    |   +-- Presenter
            |   +-- SearchPlacesPresentable.h
            |   +-- SearchPlacesPresenter.h
			|   +-- SearchPlacesPresenter.m
			|   +-- SearchPlacesPresenterDelegate.h
    |   +-- Router
            |   +-- SearchPlacesRouter.swift
    |   +-- View
            |   +-- SearchPlacesViewable.h
            |   +-- SearchPlacesViewController.h
			|   +-- SearchPlacesViewController.m
|   +-- NearbyPlaces
         
.swift tests
+-- Fixtures
+-- Mocks
+-- Modules
|   +-- Nearby Places
    |   +-- NearbyPlacesPresenterTests
|   +-- Search
    |   +-- PlacesServiceTests.swift
    |   +-- SearchPlacesInteractorTests.swift
    |   +--	SearchPlacesPresenterTests.swift
```


# Design Patterns used:

# VIPER Architecture design pattern:

**What is VIPER?**
VIPER is an application of Clean Architecture to iOS apps. The word VIPER is a backronym for View, Interactor, Presenter, Entity, and Routing. Clean Architecture divides an app’s logical structure into distinct layers of responsibility. This makes it easier to isolate dependencies (e.g. your database) and to test the interactions at the boundaries between layers:
![VIPER](Screenshots/VIPER.components.png)

- Know more about VIPER through this post http://www.objc.io/issue-13/viper.html

**Why VIPER**:

*Smaller files*:

- VIPER (without a few exceptions:) ) has very clear politics about responsibility for each component. It helps with reducing amount of code in files and putting into the right place according to a single responsibility principle. 

*Better code coverage*: 

- Smaller classes mean smaller test cases as well. Tests resort less to exotic tricks and are simpler to read. The barrier to entry to write unit tests is lower, so more developers write them. 

*Good for unit testing*:

- On the basis of VIPER principles, everything in one module is very well separated, so it creates good environment for unit testing. Look at [this](http://iosunittesting.com/tdd-with-viper/?utm_source=swifting.io) article regarding more info about TDD in VIPER.

**VIPER modules Generators** : 

If you really want to make your application based on VIPER architecture, do not even think to make it all manually. It will be a disaster! You need an automated process to create a new module.

By the way I've created an opens source tool that automate the process of generating VIPER modules. A simple OS X App for generating VIPER modules's skeleton to use them in your Objective-C/Swift projects.
You can download it now:

* [ViperCode](https://github.com/iSame7/ViperCode)
* [VIPER-Module](https://github.com/iSame7/VIPER-Module)

# Dependency Injection:

Use of VIPER architecture gives great possibility to apply dependency injection. For example, let’s consider an example of a presenter:

```swift

@interface SearchPlacesPresenter : NSObject <SearchPlacesPresentable>

@property (nonatomic, weak) id <SearchPlacesPresenterDelegate> delegate;

- (instancetype)initWithInteractor:(id <SearchPlacesInteractable>)interactor view:(id<SearchPlacesViewable>)view;

@end

@interface SearchPlacesPresenter() {
    UserLocation *userLocation;
    NSArray<NearbyPlace *> *nearbyPlaces;
}

@property (nonatomic, strong) id<SearchPlacesInteractable> interactor;

@property (nonatomic, weak) id<SearchPlacesViewable> view;

@end

@implementation SearchPlacesPresenter

- (instancetype)initWithInteractor:(id <SearchPlacesInteractable>)interactor view:(id<SearchPlacesViewable>)view {
    
    if ((self = [super init])) {
        _interactor = interactor;
        _view = view;
    }
    
    return self;
    
}

- (void)viewDidLoad {
    __weak typeof(self) weakSelf = self;
    [_interactor determineUserLocationWithCompletionHandler:^(UserLocation *result) {
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf->userLocation = result;
    }];
}

- (void)getNearbyPlacesForQuery:(NSString *)query {
    if (userLocation) {
        NSString *latAsString = [[NSNumber numberWithDouble:userLocation.lat] stringValue];
        NSString *lngAsString = [[NSNumber numberWithDouble:userLocation.lng] stringValue];        
        
        __weak typeof(self) weakSelf = self;
        [_interactor getNearbyPlacesForQuery:query AroundLocation:[NSString stringWithFormat:@"%@,%@", latAsString, lngAsString] withCompletionHandler:^(NSArray<NearbyPlace *> * _Nullable places, ErrorDetails *error) {
            __strong typeof(self) strongSelf = weakSelf;
            if (error.errorDescription != nil) {
                [[strongSelf view] showError:error];
            } else {
                strongSelf->nearbyPlaces = places;
                [[strongSelf view] update];
            }
        }];
    } else {
        [[self view] showError: [[ErrorDetails alloc] initWithErrorDescription:@"Turn Location services to discover places near you." errorTitle:@"Location services is disabled"]];
    }
}

- (void)didObtainNearbyPlaces {
    [_delegate didObtainNearbyPlaces:nearbyPlaces];
}
```

Injection in this class gave us two advantages:

* We have a better sense what’s going on in this code. We see immediately what dependencies our class has
* On the other hand, our class is prepared for unit testing

* When using VIPER architecture a good practice is to use DI in every component. i will show in Unit Test section a few examples how this approach can really help us during testing. 
* The project is using `Swinject` to manage the dependency injection. 


# Unit testing:

We started from testing interactor and presenter, because interactor contains main business logic and presenter contains logic responsible for preparing data before displaying. These components seems more critical than others.

Libraries/Frameworks i used for unit tests and TDD:

* XCTest


In VIPER every component of a module is strictly separated what creates a very friendly scenario for adopting unit tests in terms of single responsibility principle:

let’s consider an example of a presenter of List Jobs Module:

by separating components in our test we can focus only on testing responsibility of interactor. The others components which talk with interactor are just mocked.

How does it look like in perspective of code?

```swift
class SearchPlacesPresenterTests: XCTestCase {
    // MARK: - Test Properties
    
    private let searchPlacesInteractorMock = SearchPlacesInteractorMock()
    private let searchPlacesPresenterDelegateSpy = SearchPlacesPresenterDelegateSpy()
    private let searchPlacesViewControllerMock = SearchPlacesViewControllerMock()
    private let nearbyPlacesMock = [NearbyPlace(name: "Concerto Koffie",
                                                category: "cafe", address: "Utrechtsestraat 52, 1017 VP Amsterdam",
                                                icon: "https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png",
                                                position: CLLocationCoordinate2DMake(52.36344889999999, 4.898373800000001)),
                                    NearbyPlace(name: "Café Blond",
                                                category: "cafe",
                                                address: "Ferdinand Bolstraat 44H, 1072 LL Amsterdam",
                                                icon: "https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png",
                                                position: CLLocationCoordinate2DMake(52.3560132, 4.8903867))
    ]
    private var sut: SearchPlacesPresenter!
    
    // MARK: - Test Life cycle
    
    override func setUp() {
        super.setUp()
        
        sut = SearchPlacesPresenter(interactor: searchPlacesInteractorMock, view: searchPlacesViewControllerMock)
    }
    
    override func tearDown() {
        sut = nil
        
        super.tearDown()
    }
    
    // MARK: - Tests
    
    func testGetNearbyPlacesAndInvokeViewUpdateWhenNewPlacesArrive() {
        // Given
        searchPlacesInteractorMock.stubbedDeterminUserLocationCompletion = (UserLocation(lat: 52.364539, lng: 4.899759))
        searchPlacesInteractorMock.stubbedGetNearbyPlacesCompletion  = (nearbyPlacesMock, nil)
        
        // When
        sut.viewDidLoad()
        sut.getNearbyPlaces(forQuery: "Cafe")
        
        // Then
        XCTAssertTrue(searchPlacesViewControllerMock.invokedUpdate)
    }
    
    func testGetNeearbyPlacesAndInvokeErrorWhenNoPlacesArrive() {
        // Given
        searchPlacesInteractorMock.stubbedDeterminUserLocationCompletion = (UserLocation(lat: 52.364539, lng: 4.899759))
        searchPlacesInteractorMock.stubbedGetNearbyPlacesCompletion  = (nil, BeatError.JSONParsing.errorDetails())
        
        // When
        sut.viewDidLoad()
        sut.getNearbyPlaces(forQuery: "Cafe")
        
        // Then
        XCTAssertTrue(searchPlacesViewControllerMock.invokedShowError)
    }
    
    func testGetNearbyPlacesAndInvokeErrorWhenThereIsNoUserLocation() {
        // Given
        searchPlacesInteractorMock.stubbedGetNearbyPlacesCompletion  = (nearbyPlacesMock, nil)

        // When
         sut.viewDidLoad()
         sut.getNearbyPlaces(forQuery: "Cafe")
         
         // Then
         XCTAssertTrue(searchPlacesViewControllerMock.invokedShowError)
    }
    
    func testDidObtainNearbyPlaces() {
        // Given
        sut.delegate = searchPlacesPresenterDelegateSpy
        
        // When
        sut.didObtainNearbyPlaces()
        
        // Then
        XCTAssertTrue(searchPlacesPresenterDelegateSpy.invokeDidObtainNearbyPlaces)
    }
}
```
