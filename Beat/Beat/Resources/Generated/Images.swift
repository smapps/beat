// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import UIKit.UIImage

// MARK: - Image Asset Catalog

internal enum Image {
  internal static let back = "Back"
  internal static let pinSelected = "Pin-Selected"
  internal static let pinUnselected = "Pin-Unselected"
  internal static let pin = "Pin"
  internal static let logo = "logo"
}

// MARK: - Implementation Details

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  internal var image: UIImage {
    let bundle = Bundle(for: BundleToken.self)
    let image = UIImage(named: name, in: bundle, compatibleWith: nil)
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

internal extension UIImage {
  convenience init!(asset: ImageAsset) {
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
  }
}

private final class BundleToken {}
