//
//  Place.swift
//  Beat
//
//  Created by Sameh Mabrouk on 26/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

struct Place: Decodable {
    let id: String
    let name: String
    let icon: String
    let formattedAddress: String
    let types: [String]
    let geometry: Geometry
}

struct Geometry: Decodable {
    let location: Location
}
