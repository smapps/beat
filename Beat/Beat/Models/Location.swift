//
//  Location.swift
//  Beat
//
//  Created by Sameh Mabrouk on 04/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

struct Location: Decodable {
    let lat: Double
    let lng: Double
}
