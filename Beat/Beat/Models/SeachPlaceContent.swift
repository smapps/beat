//
//  SeachPlaceContent.swift
//  Beat
//
//  Created by Sameh Mabrouk on 02/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

struct SearchPlaceContent: Decodable {
    let results: [Place]
}
