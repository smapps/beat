//
//  UserLocation.swift
//  Beat
//
//  Created by Sameh Mabrouk on 02/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

typealias UserLocationBlock = (UserLocation) -> Void

@objc public class UserLocation: NSObject {
    @objc public var lat: Double
    @objc public var lng: Double
    
    @objc public init(lat: Double, lng: Double) {
        self.lat = lat
        self.lng = lng
    }
}
