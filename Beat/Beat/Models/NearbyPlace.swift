//
//  NearbyPlace.swift
//  Beat
//
//  Created by Sameh Mabrouk on 02/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

import CoreLocation.CLLocation

@objc open class NearbyPlace: NSObject {
    // MARK: - Properties
    
    @objc private(set) var name: String
    @objc private(set) var category: String?
    @objc private(set) var address: String
    @objc private(set) var icon: String
    @objc private(set) var position: CLLocationCoordinate2D
    
    // MARK: - Init
    
    @objc init(name: String, category: String?, address: String, icon: String, position: CLLocationCoordinate2D) {
        self.name = name
        self.category = category
        self.address = address
        self.icon = icon
        self.position = position
    }
}
