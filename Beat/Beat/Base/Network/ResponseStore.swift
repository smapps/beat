//
//  ResponseStore.swift
//  Beat
//
//  Created by Sameh Mabrouk on 03/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

import Foundation
import Alamofire

public protocol ResponseStore {
    
    func data(for: URLRequest) -> Data
    
    func data(for: URL, withParameters params: Parameters?) -> Data
    
}
