//
//  NetworkRouter.swift
//  Beat
//
//  Created by Sameh Mabrouk on 27/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import Alamofire

enum NetworkRouter: URLRequestConvertible {
    case fetchPlaces(query: String, location: String)
    
    static let baseURLString = Constants.App.baseURL
    
    var method: HTTPMethod {
        switch self {
        case .fetchPlaces:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .fetchPlaces:
            return "place/textsearch/json"
        }
    }
    
    var parameters: [String: AnyObject]? {
        switch self {
        case let .fetchPlaces(query, location):
            return ["query": query,
                    "location": location,
                    "radius": "1000",
                    "type": "cafe, store",
                    "key": Constants.ThirdParty.Google.Maps.apiKey
                ] as [String: AnyObject]?
            
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try NetworkRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        print("URL: \(String(describing: urlRequest.url))")
        return urlRequest
    }
}
