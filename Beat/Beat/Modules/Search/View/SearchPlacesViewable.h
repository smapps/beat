//
//  SearchPlacesViewable.h
//  Beat
//
//  Created by Sameh Mabrouk on 30/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ErrorDetails;

@protocol SearchPlacesViewable <NSObject>

- (void)update;
- (void)showError:(ErrorDetails *)error;

@end
