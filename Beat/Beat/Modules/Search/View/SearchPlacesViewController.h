//
//  SearchPlacesViewController.h
//  Beat
//
//  Created by Sameh Mabrouk on 30/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchPlacesViewable.h"
#import "SearchPlacesPresentable.h"

NS_ASSUME_NONNULL_BEGIN

@protocol NavigationControllable;

@interface SearchPlacesViewController : UIViewController <SearchPlacesViewable, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *pinImageView;
@property (weak, nonatomic) IBOutlet UILabel *searchLabel;


@property (nonatomic, strong) id<SearchPlacesPresentable> presenter;

+ (SearchPlacesViewController *)instantiate;

@end

NS_ASSUME_NONNULL_END
