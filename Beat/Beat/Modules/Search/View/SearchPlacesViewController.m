//
//  SearchPlacesViewController.m
//  Beat
//
//  Created by Sameh Mabrouk on 30/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

#import "SearchPlacesViewController.h"
#import "Beat-Swift.h"

static NSString *SearchPlacestViewControllerIdentifier = @"SearchPlacesViewController";

@interface SearchPlacesViewController ()

@end

@implementation SearchPlacesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    [_presenter viewDidLoad];
    
    [_activityIndicator setHidden:YES];
    
    self.searchBar.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController.navigationBar setHidden:YES];
}

#pragma mark - UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    if (searchBar.text.length > 0) {
        [_presenter getNearbyPlacesForQuery:searchBar.text];
        
        [_pinImageView setHidden:YES];
        [_searchLabel setHidden:YES];
        
        [_activityIndicator setHidden:NO];
        [_activityIndicator startAnimating];
        [_activityIndicator hidesWhenStopped];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
}

#pragma mark - Helpers

+ (SearchPlacesViewController *)instantiate {
    UIStoryboard *storyboard = [self storyboard];
    SearchPlacesViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:SearchPlacestViewControllerIdentifier];
    
    return viewController;
}

+ (UIStoryboard *)storyboard {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SearchPlaces"
                                                         bundle:[NSBundle mainBundle]];
    
    return storyboard;
}

#pragma mark - SearchPlacesViewable

- (void)update {
    [_presenter didObtainNearbyPlaces];
    [self updateUI];
}

- (void)showError:(ErrorDetails *)error {
    [InAppNotifications showNotificationWithType:InAppNotifications.error title:error.errorTitle message:error.errorDescription dismissDelay:3.0 completion:^{
        [self updateUI];
    }];
}

- (void)updateUI {
    [_activityIndicator stopAnimating];
    [_activityIndicator setHidden:YES];
    [_pinImageView setHidden:NO];
    [_searchLabel setHidden:NO];
}

@end
