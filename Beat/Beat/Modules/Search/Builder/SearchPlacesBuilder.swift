//
//  SearchPlacesBuilder.swift
//  Beat
//
//  Created by Sameh Mabrouk on 31/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import Swinject
import Alamofire
import CoreLocation

class SearchPlacesBuilder: ModuleBuildable {
    // MARK: - Properties
    
    private let container: Container
    
    // MARK: - Init
    
    init(container: Container) {
        self.container = container
        Container.loggingFunction = nil
    }
    
    func buildModule() -> Beat.Module? {
        registerView()
        registerLocationService()
        registerNetwork()
        registerPlacesService()
        registerInteractor()
        registerPresenter()
        
        guard let searchPlacesViewController = container.resolve(SearchPlacesViewable.self) as? UIViewController  else { return nil }
        
        let navController = UINavigationController(rootViewController: searchPlacesViewController)
        return Beat.Module(viewController: navController)
    }
    
    private func registerView() {
        container.register(SearchPlacesViewable.self, factory: { _ in
            SearchPlacesViewController.instantiate()
        }).initCompleted({ (r, view) in
            if let searchPlacesViewController = view as? SearchPlacesViewController {
                searchPlacesViewController.presenter = r.resolve(SearchPlacesPresentable.self)!
            }
        }).inObjectScope(.container)
    }
    
    private func registerLocationService() {
        container.register(CLLocationManager.self, factory: { _ in CLLocationManager() }).initCompleted({ _, manager in
            manager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            manager.distanceFilter = 5
            manager.allowsBackgroundLocationUpdates = true
            manager.activityType = .fitness
            if #available(iOS 11.0, *) {
                manager.showsBackgroundLocationIndicator = false
            }
        }).inObjectScope(.container)
        
        container.register(LocationServiceChecking.self) { r in
            LocationService(locationManager: r.resolve(CLLocationManager.self)!)
        }
    }
    
    func registerNetwork() {
        container.register(SessionManager.self, factory: { r in
            SessionManager()
        }).inObjectScope(.container)
        
        container.register(RequestRetrier.self, factory: { r in
            NetworkRequestRetrier()
        }).inObjectScope(.container)
    }
    
    private func registerPlacesService() {
        container.register(PlacesFetching.self) { r in
            PlacesService(sessionManager: r.resolve(SessionManager.self)!, requestRetrier: r.resolve(RequestRetrier.self)!)
        }
    }
    
    private func registerInteractor() {
        container.register(SearchPlacesInteractable.self) { r in
            SearchPlacesInteractor(placesService: r.resolve(PlacesFetching.self)!, locationService: r.resolve(LocationServiceChecking.self)!)
        }
    }
    
    private func registerPresenter() {
        container.register(SearchPlacesPresentable.self, factory: { r in
            SearchPlacesPresenter(interactor: r.resolve(SearchPlacesInteractable.self)!, view: r.resolve(SearchPlacesViewable.self)!)
        }).initCompleted({ (r, presenter) in
            if let presenter = presenter as? SearchPlacesPresenter {
                presenter.delegate = r.resolve(SearchPlacesRouter.self)!
            }
        }).inObjectScope(.container)
    }
}
