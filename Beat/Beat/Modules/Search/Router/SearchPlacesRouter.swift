//
//  SearchPlacesRouter.swift
//  Beat
//
//  Created by Sameh Mabrouk on 31/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import UIKit

class SearchPlacesRouter: Router {
    // MARK: - Properties
    
    private let searchPlacesBuilder: ModuleBuildable?
    
    var neabyPlacesRouter: NearbyPlacesRouter?
    
    var window: UIWindow?
    
    // MARK: - Init
    
    init(searchPlacesBuilder: ModuleBuildable?) {
        self.searchPlacesBuilder = searchPlacesBuilder
    }
    
    // MARK: - Router
    
    override func start() {
        guard let window = window else {
            return
        }
        
        if let searchPlacesBuilder = searchPlacesBuilder {
            window.rootViewController = searchPlacesBuilder.buildModule()?.viewController
            window.makeKeyAndVisible()
            self.window = window
        }
    }
}

// MARK: - SearchPlacesPresenterDelegate

extension SearchPlacesRouter: SearchPlacesPresenterDelegate {
    func didObtain(_ nearbyPlaces: [NearbyPlace]) {
        if let nearbyPlacesRouter = neabyPlacesRouter {
            nearbyPlacesRouter.nearbyPlaces = nearbyPlaces
            nearbyPlacesRouter.delegate = self
            let navigationController = window?.rootViewController as? UINavigationController
            nearbyPlacesRouter.rootViewController = navigationController
            startChildRouter(nearbyPlacesRouter)
        }
    }
}

extension SearchPlacesRouter: NearbyPlacesRouterDelegate {
    func nearbyPlacesRouterDidFinish(_ nearbyPlacesRouter: NearbyPlacesRouter) {
        nearbyPlacesRouter.stop()
        removeChildRouter(nearbyPlacesRouter)
    }
}
