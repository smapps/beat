//
//  SearchPlacesPresenter.m
//  Beat
//
//  Created by Sameh Mabrouk on 30/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

#import "SearchPlacesPresenter.h"
#import "Beat-Swift.h"

@interface SearchPlacesPresenter() {
    UserLocation *userLocation;
    NSArray<NearbyPlace *> *nearbyPlaces;
}

@property (nonatomic, strong) id<SearchPlacesInteractable> interactor;

@property (nonatomic, weak) id<SearchPlacesViewable> view;

@end

@implementation SearchPlacesPresenter

- (instancetype)initWithInteractor:(id <SearchPlacesInteractable>)interactor view:(id<SearchPlacesViewable>)view {
    
    if ((self = [super init])) {
        _interactor = interactor;
        _view = view;
    }
    
    return self;
    
}

- (void)viewDidLoad {
    __weak typeof(self) weakSelf = self;
    [_interactor determineUserLocationWithCompletionHandler:^(UserLocation *result) {
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf->userLocation = result;
    }];
}

- (void)getNearbyPlacesForQuery:(NSString *)query {
    if (userLocation) {
        NSString *latAsString = [[NSNumber numberWithDouble:userLocation.lat] stringValue];
        NSString *lngAsString = [[NSNumber numberWithDouble:userLocation.lng] stringValue];        
        
        __weak typeof(self) weakSelf = self;
        [_interactor getNearbyPlacesForQuery:query AroundLocation:[NSString stringWithFormat:@"%@,%@", latAsString, lngAsString] withCompletionHandler:^(NSArray<NearbyPlace *> * _Nullable places, ErrorDetails *error) {
            __strong typeof(self) strongSelf = weakSelf;
            if (error.errorDescription != nil) {
                [[strongSelf view] showError:error];
            } else {
                strongSelf->nearbyPlaces = places;
                [[strongSelf view] update];
            }
        }];
    } else {
        [[self view] showError: [[ErrorDetails alloc] initWithErrorDescription:@"Turn Location services to discover places near you." errorTitle:@"Location services is disabled"]];
    }
}

- (void)didObtainNearbyPlaces {
    [_delegate didObtainNearbyPlaces:nearbyPlaces];
}


@end
