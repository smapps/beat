//
//  SearchPlacesPresenterDelegate.h
//  Beat
//
//  Created by Sameh Mabrouk on 03/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SearchPlacesPresenterDelegate <NSObject>

- (void)didObtainNearbyPlaces:(NSArray<NearbyPlace *> *)nearbyPlaces;

@end
