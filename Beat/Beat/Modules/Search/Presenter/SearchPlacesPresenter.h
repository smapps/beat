//
//  SearchPlacesPresenter.h
//  Beat
//
//  Created by Sameh Mabrouk on 30/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchPlacesPresentable.h"
#import "SearchPlacesInteractable.h"
#import "SearchPlacesViewable.h"
#import "SearchPlacesPresenterDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchPlacesPresenter : NSObject <SearchPlacesPresentable>

@property (nonatomic, weak) id <SearchPlacesPresenterDelegate> delegate;

- (instancetype)initWithInteractor:(id <SearchPlacesInteractable>)interactor view:(id<SearchPlacesViewable>)view;

@end

NS_ASSUME_NONNULL_END
