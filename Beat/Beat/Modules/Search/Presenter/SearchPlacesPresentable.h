//
//  SearchPlacesPresentable.h
//  Beat
//
//  Created by Sameh Mabrouk on 30/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SearchPlacesPresentable <NSObject>

- (void)viewDidLoad;

- (void)getNearbyPlacesForQuery:(NSString*)query;
- (void)didObtainNearbyPlaces;

@end
