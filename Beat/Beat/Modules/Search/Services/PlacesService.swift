//
//  PlacesService.swift
//  Beat
//
//  Created by Sameh Mabrouk on 31/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import Alamofire
import MapKit

@objc protocol PlacesFetching {
    @objc optional func fetchNearbyPlaces(query: String, location: String, completion: @escaping ([NearbyPlace]?, ErrorDetails?) -> Void)
}

class PlacesService: PlacesFetching {
    // MARK: - Properties
    
    private let sessionManager: SessionManagerProtocol
    private let requestRetrier: RequestRetrier
    
    // MARK: - Init
    
    init(sessionManager: SessionManagerProtocol, requestRetrier: RequestRetrier) {
        self.sessionManager = sessionManager
        self.requestRetrier = requestRetrier
    }
    
    func fetchNearbyPlaces(query: String, location: String, completion: @escaping ([NearbyPlace]?, ErrorDetails?) -> Void) {
        sessionManager.request(NetworkRouter.fetchPlaces(query: query, location: location)).responseData { response in
            let jsonDecoder = JSONDecoder()
            jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
            let content: Result<SearchPlaceContent> = jsonDecoder.decodeResponse(from: response)
            
            var fetchNearbyPlacesErrorDetails = ErrorDetails(errorDescription: nil, errorTitle: nil)
            if let error = content.error as? BeatError {
                fetchNearbyPlacesErrorDetails = error.errorDetails()
            }
    
            var nearbyPlaces = [NearbyPlace]()
            content.value?.results.forEach({ place in
                nearbyPlaces.append(NearbyPlace(name: place.name, category: place.types.first, address: place.formattedAddress, icon: place.icon, position: CLLocationCoordinate2DMake(place.geometry.location.lat, place.geometry.location.lng)))
            })
            completion(nearbyPlaces, fetchNearbyPlacesErrorDetails)
        }
    }
}
