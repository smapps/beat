//
//  LocationService.swift
//  Beat
//
//  Created by Sameh Mabrouk on 02/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import MapKit

@objc protocol LocationServiceChecking {
    func requestAuthorization()
    func requestUserLocation(completion: @escaping UserLocationBlock)
}

class LocationService: NSObject, LocationServiceChecking {
    // MARK: - Properties
    
    private let locationManager: CLLocationManager!
    private var status: CLAuthorizationStatus!
    private var locationUpdateCompletion: UserLocationBlock!
    
    // MARK: - Init
    
    required init(locationManager: CLLocationManager) {
        self.locationManager = locationManager
        super.init()
        locationManager.delegate = self
    }
}

extension LocationService {
    
    func requestAuthorization() {
        if CLLocationManager.locationServicesEnabled() {
            print("location services available")
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
                status = CLAuthorizationStatus.notDetermined
            case .denied:
                status = CLAuthorizationStatus.denied
                //opens phone Settings so user can authorize permission
                guard let validSettingsURL: URL = URL(string: UIApplication.openSettingsURLString) else { return }
                UIApplication.shared.open(validSettingsURL, options: [:], completionHandler: nil)
            case .authorizedWhenInUse:
                status = CLAuthorizationStatus.authorizedWhenInUse
            case .authorizedAlways:
                status = CLAuthorizationStatus.authorizedAlways
            case .restricted:
                status = CLAuthorizationStatus.restricted
                guard let validSettingsURL: URL = URL(string: UIApplication.openSettingsURLString) else { return }
                UIApplication.shared.open(validSettingsURL, options: [:], completionHandler: nil)
            @unknown default:
                assertionFailure("Uknown authorizationStatus")
            }
        }
        else {
            print("location services NOT available")
        }
    }
    
    func requestUserLocation( completion: @escaping UserLocationBlock) {
        locationUpdateCompletion = completion
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
}

//MARK: CLLocationManagerDelegate

extension LocationService: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        
        locationUpdateCompletion(UserLocation(lat: location.coordinate.latitude, lng: location.coordinate.longitude))
        locationManager.delegate = nil
    }
}
