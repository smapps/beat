//
//  SearchPlacesInteractor.m
//  Beat
//
//  Created by Sameh Mabrouk on 30/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

#import "Beat-Swift.h"
#import "SearchPlacesInteractor.h"

@interface SearchPlacesInteractor()

@property (nonatomic, strong) id<PlacesFetching> placesService;
@property (nonatomic, strong) id<LocationServiceChecking> locationService;

@end

@implementation SearchPlacesInteractor

- (instancetype)initWithPlacesService:(id<PlacesFetching>)placesService locationService:(id<LocationServiceChecking>)locationService {
    if ((self = [super init]))
    {
        _placesService = placesService;
        _locationService = locationService;
    }
    
    return self;
}

- (void)determineUserLocationWithCompletionHandler:(void (^)(UserLocation *))completion {
    [_locationService requestAuthorization];
    [_locationService requestUserLocationWithCompletion:^(UserLocation * userLocation) {
        completion(userLocation);
    }];
}

- (void)getNearbyPlacesForQuery:(NSString *)query AroundLocation:(NSString *)location withCompletionHandler:(void (^ _Nonnull)(NSArray<NearbyPlace *>* _Nullable places, ErrorDetails *error))completion {
    [_placesService fetchNearbyPlacesWithQuery:query location:location completion:^(NSArray<NearbyPlace *> *places, ErrorDetails *error) {
        completion(places, error);
    }];
}

@end
