//
//  SearchPlacesInteractor.h
//  Beat
//
//  Created by Sameh Mabrouk on 30/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchPlacesInteractable.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PlacesFetching;
@protocol LocationServiceChecking;

@interface SearchPlacesInteractor : NSObject <SearchPlacesInteractable>

- (instancetype)initWithPlacesService:(id <PlacesFetching>)placesService locationService: (id<LocationServiceChecking>)locationService;

@end

NS_ASSUME_NONNULL_END
