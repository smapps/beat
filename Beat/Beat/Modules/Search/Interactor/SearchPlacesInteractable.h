//
//  SearchPlacesInteractable.h
//  Beat
//
//  Created by Sameh Mabrouk on 30/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserLocation;
@class NearbyPlace;
@class ErrorDetails;

@protocol SearchPlacesInteractable <NSObject>

- (void)determineUserLocationWithCompletionHandler: (void(^_Nonnull)(UserLocation* _Nullable result))completion;
- (void)getNearbyPlacesForQuery:(NSString *_Nullable)query AroundLocation:(NSString *_Nullable)location withCompletionHandler:(void (^ _Nonnull)(NSArray<NearbyPlace *>* _Nullable places, ErrorDetails * _Nullable error))completion;

@end
