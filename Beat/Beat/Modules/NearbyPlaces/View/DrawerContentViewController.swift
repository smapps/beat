//
//  DrawerContentViewController.swift
//  Beat
//
//  Created by Sameh Mabrouk on 29/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import UIKit

protocol DrawerContentViewControllerDelegate: class {
    func drawerDidSelectRowAt(index: Int)
}

class DrawerContentViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var gripperView: UIView!
    @IBOutlet var topSeparatorView: UIView!
    @IBOutlet var bottomSeperatorView: UIView!
    
    @IBOutlet var gripperTopConstraint: NSLayoutConstraint!
    
    // We adjust our 'header' based on the bottom safe area using this constraint
    @IBOutlet var headerSectionHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: DrawerContentViewControllerDelegate?
    
    private var selectedIndex: IndexPath!
    
    private var drawerBottomSafeArea: CGFloat = 0.0 {
        didSet {
            self.loadViewIfNeeded()
            
            // We'll configure our UI to respect the safe area. In our small demo app, we just want to adjust the contentInset for the tableview.
            tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: drawerBottomSafeArea, right: 0.0)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        gripperView.layer.cornerRadius = 2.5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // You must wait until viewWillAppear -or- later in the view controller lifecycle in order to get a reference to Roller via self.parent for customization.
        
        // UIFeedbackGenerator is only available iOS 10+. Since Roller works back to iOS 9, the .feedbackGenerator property is "Any" and managed internally as a feedback generator.
        if #available(iOS 10.0, *)
        {
            let feedbackGenerator = UISelectionFeedbackGenerator()
            rollerViewController?.feedbackGenerator = feedbackGenerator
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // The bounce here is optional, but it's done automatically after appearance as a demonstration.
        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(bounceDrawer), userInfo: nil, repeats: false)
        
        selectedIndex = IndexPath(row: 0, section: 0)
        tableView.selectRow(at: selectedIndex, animated: true, scrollPosition: .none)
        tableView(tableView, didSelectRowAt: selectedIndex)
    }
    
    @objc fileprivate func bounceDrawer() {
        
        // We can 'bounce' the drawer to show users that the drawer needs their attention. There are optional parameters you can pass this method to control the bounce height and speed.
        rollerViewController?.bounceDrawer()
    }
}

// MARK: - RollerDrawerViewControllerDelegate

extension DrawerContentViewController: RollerDrawerViewControllerDelegate {
    
    func collapsedDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat {
        // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
        return 68.0 + (rollerViewController?.currentDisplayMode == .drawer ? bottomSafeArea : 0.0)
    }
    
    func partialRevealDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat {
        // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
        return 264.0 + (rollerViewController?.currentDisplayMode == .drawer ? bottomSafeArea : 0.0)
    }
    
    func supportedDrawerPositions() -> [RollerPosition] {
        return RollerPosition.all // You can specify the drawer positions you support. This is the same as: [.open, .partiallyRevealed, .collapsed, .closed]
    }
    
    // This function is called by Roller anytime the size, drawer position, etc. changes. It's best to customize your VC UI based on the bottomSafeArea here (if needed). Note: You might also find the `RollerSafeAreaInsets` property on Roller useful to get Roller's current safe area insets in a backwards compatible (with iOS < 11) way. If you need this information for use in your layout, you can also access it directly by using `drawerDistanceFromBottom` at any time.
    func drawerPositionDidChange(drawer: RollerViewController, bottomSafeArea: CGFloat) {
        // We want to know about the safe area to customize our UI. Our UI customization logic is in the didSet for this variable.
        drawerBottomSafeArea = bottomSafeArea
        
        /*
         Some explanation for what is happening here:
         1. Our drawer UI needs some customization to look 'correct' on devices like the iPhone X, with a bottom safe area inset.
         2. We only need this when it's in the 'collapsed' position, so we'll add some safe area when it's collapsed and remove it when it's not.
         3. These changes are captured in an animation block (when necessary) by Roller, so these changes will be animated along-side the drawer automatically.
         */
        if drawer.drawerPosition == .collapsed {
            headerSectionHeightConstraint.constant = 68.0 + drawerBottomSafeArea
        } else {
            headerSectionHeightConstraint.constant = 68.0
        }
        
        // Handle tableview scrolling / searchbar editing
        
        tableView.isScrollEnabled = drawer.drawerPosition == .open || drawer.currentDisplayMode == .panel
        
        if drawer.drawerPosition != .open {
            searchBar.resignFirstResponder()
        }
        
        if drawer.currentDisplayMode == .panel {
            topSeparatorView.isHidden = drawer.drawerPosition == .collapsed
            bottomSeperatorView.isHidden = drawer.drawerPosition == .collapsed
        } else {
            topSeparatorView.isHidden = false
            bottomSeperatorView.isHidden = true
        }
    }
    
    /// This function is called when the current drawer display mode changes. Make UI customizations here.
    func drawerDisplayModeDidChange(drawer: RollerViewController) {
        
        print("Drawer: \(drawer.currentDisplayMode)")
        gripperTopConstraint.isActive = drawer.currentDisplayMode == .drawer
    }
}

// MARK: - UISearchBarDelegate

extension DrawerContentViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        rollerViewController?.setDrawerPosition(position: .open, animated: true)
    }
}

// MARK: - UITableViewDataSource

extension DrawerContentViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return rollerViewController?.presenter.numberOfSections() ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rollerViewController?.presenter.numberOfRowsInSection() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell", for: indexPath) as! PlaceUITableViewCell
        if indexPath == selectedIndex {
            cell.iconBackgroundColor = .binSelectedColor
        } else {
            cell.iconBackgroundColor = .binUnselectedColor
        }
        
        let placeViewModel = rollerViewController?.presenter.itemAtIndex(index: indexPath.row, in: 0)
        cell.viewModel = placeViewModel
        return cell
    }
}

// MARK: - UITableViewDelegate

extension DrawerContentViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        rollerViewController?.setDrawerPosition(position: .collapsed, animated: true)
        delegate?.drawerDidSelectRowAt(index: indexPath.row)
        
        updateCellAndIndex(indexPath)
    }
}

// MARK: - PrimaryContentViewControllerDelegate

extension DrawerContentViewController: PrimaryContentViewControllerDelegate {
    func primaryDidSelectMarkerAt(index: Int) {
        let index = IndexPath(row: index, section: 0)
        tableView.selectRow(at: index, animated: true, scrollPosition: .none)
        updateCellAndIndex(index)
    }
}

// MARK: - Helpers

private extension DrawerContentViewController {
    func updateCellAndIndex(_ index: IndexPath) {
        selectedIndex = index
        if let cell = tableView.cellForRow(at: selectedIndex) as? PlaceUITableViewCell {
            cell.iconBackgroundColor = .binSelectedColor
        }
        
        tableView.reloadData()
    }
}
