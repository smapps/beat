//
//  PrimaryContentViewController.swift
//  Beat
//
//  Created by Sameh Mabrouk on 29/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit

protocol PrimaryContentViewControllerDelegate: class {
    func primaryDidSelectMarkerAt(index: Int)
}

class PrimaryContentViewController: UIViewController {
    // MARK: - IBOutlets
    
    @IBOutlet var mapView: GMSMapView!
    
    weak var delegate: PrimaryContentViewControllerDelegate?
    
    private var markers: [GMSMarker]! {
        didSet {
            markers.forEach { $0.map = mapView }
            
            let firstMarker = markers.first
            firstMarker?.icon = UIImage(named: Image.pinUnselected)
            mapView.selectedMarker = firstMarker
            
            guard let firstMarkerPosition = firstMarker?.position else { return }
            
            repositionCameraAt(position: firstMarkerPosition)
        }
    }
    
    // MARK: - Life cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        rollerViewController?.displayMode = .automatic
        
        setupMapView()
    }
}

// MARK: - GMSMapViewDelegate

extension PrimaryContentViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        guard let presenter = rollerViewController?.presenter else { return }
        
        guard let nearestPlaceMarker = presenter.findNearestPlaceTo(position: position.target) else { return }
        
        selectMarkerAndUnselecteOthers(nearestPlaceMarker)
        
        guard let selectedMarkerIndex = markers.firstIndex(of: nearestPlaceMarker) else { return }

        delegate?.primaryDidSelectMarkerAt(index: selectedMarkerIndex)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        selectMarkerAndUnselecteOthers(marker)
        
        guard let selectedMarkerIndex = markers.firstIndex(of: marker) else { return false }
        
        delegate?.primaryDidSelectMarkerAt(index: selectedMarkerIndex)
        
        return false
    }
}

// MARK: - Helpers

private extension PrimaryContentViewController {
    
    func setupMapView() {
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        
        guard let presenter = rollerViewController?.presenter else { return }
        
        markers = presenter.getPlacesMarkers()
    }
    
    func selectMarkerAndUnselecteOthers(_ marker: GMSMarker) {
        marker.icon = UIImage(named: Image.pinSelected)
        
        markers.forEach {
            if $0.position.latitude != marker.position.latitude && $0.position.longitude != marker.position.longitude {
                $0.icon = UIImage(named: Image.pinUnselected)
            }
        }
    }
    
    func repositionCameraAt(position: CLLocationCoordinate2D) {
        mapView.camera = GMSCameraPosition(target: position, zoom: 16, bearing: 0, viewingAngle: 0)
    }
}

// MARK: - DrawerContentViewControllerDelegate

extension PrimaryContentViewController: DrawerContentViewControllerDelegate {
    func drawerDidSelectRowAt(index: Int) {
        let marker = markers[index]
        selectMarkerAndUnselecteOthers(marker)
        repositionCameraAt(position: marker.position)
    }
}
