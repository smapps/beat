//
//  PlaceUITableViewCell.swift
//  Beat
//
//  Created by Sameh Mabrouk on 30/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import UIKit
import Nuke

class PlaceUITableViewCell: UITableViewCell {
    // MARK: - IBOutlets

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var iconBackground: UIImageView!
    @IBOutlet weak var cornerBackgroundView: UIView! {
        didSet {
            cornerBackgroundView.addGradient(colors: [UIColor.white.withAlphaComponent(0).cgColor, UIColor.beatGrey.withAlphaComponent(1).cgColor])
//            backgroundImageView.layer.cornerRadius = 39.0
//            backgroundImageView.layer.shadowPath =
//            UIBezierPath(roundedRect: backgroundImageView.bounds,
//            cornerRadius: 39).cgPath
//            backgroundImageView.layer.shadowColor = UIColor.beatShadowColor.cgColor
//            backgroundImageView.layer.shadowOffset = CGSize(width: 100, height: 100)
//            backgroundImageView.layer.shadowOpacity = 1
//            backgroundImageView.layer.masksToBounds = false
//            backgroundImageView.backgroundColor = .red
        }
    }
    
    // MARK: - Properties
    
    var viewModel: PlaceCellViewModel? {
        didSet {
            nameLabel.text = viewModel?.name
            categoryLabel.text = viewModel?.category
            addressLabel.text = viewModel?.address
            
            if let imagePath = viewModel?.icon, let imageURL = URL(string: imagePath) {
                Nuke.loadImage(with: imageURL, into: icon, progress: nil) { [weak self] _ in
                    self?.icon.setImageColor(color: UIColor.white)
                }
            }
            
        }
    }
    
    var iconBackgroundColor: UIColor? {
        didSet {
            icon.backgroundColor = iconBackgroundColor
        }
    }    
}
extension UIImageView {
  func setImageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}
