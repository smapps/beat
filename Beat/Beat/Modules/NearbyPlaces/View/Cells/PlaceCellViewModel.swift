//
//  PlaceCellViewModel.swift
//  Beat
//
//  Created by Sameh Mabrouk on 03/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

import UIKit

struct PlaceCellViewModel {
    let name: String
    let category: String
    let address: String
    let icon: String
}
