//
//  PlaceMarker.swift
//  Beat
//
//  Created by Sameh Mabrouk on 03/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

import UIKit
import GoogleMaps

class PlaceMarker: GMSMarker {
    let place: NearbyPlace
    
    init(place: NearbyPlace) {
        self.place = place
        super.init()
        
        position = place.position
        icon = UIImage(named: "Pin-Unselected")
        groundAnchor = CGPoint(x: 0.5, y: 1)
        appearAnimation = .pop
    }
}
