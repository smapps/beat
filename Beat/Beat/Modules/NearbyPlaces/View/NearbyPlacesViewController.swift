//
//  NearbyPlacesViewController.swift
//  Beat
//
//  Created by Sameh Mabrouk on 26/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import UIKit

class NearbyPlacesViewController: RollerViewController {
    // MARK: - Properties
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        addBackBarButtonItem()
    }
}

// MARK: - StoryboardInstantiatable

extension NearbyPlacesViewController: StoryboardInstantiatable {
    static var instantiateType: StoryboardInstantiateType {
        return .initial
    }
    
    static var storyboardName: String {
        return StoryboardName.nearbyPlaces.rawValue
    }
}

// MARK: - NearbyPlacesViewable

extension NearbyPlacesViewController: NearbyPlacesViewable {}

private extension NearbyPlacesViewController {
    func setupNavBar() {
        title = "Nearby places"
        
        let navBar = navigationController?.navigationBar
        navBar?.isHidden = false
        
        navBar?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.navBarTitleColor]
    }
}


// MARK: - BackBarButtonActionHandling
extension NearbyPlacesViewController: BackBarButtonActionHandling {
    func backButtonTapped() {
        presenter.backButtonTapped()
    }
}
