//
//  NearbyPlacesBuilder.swift
//  Beat
//
//  Created by Sameh Mabrouk on 28/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import Alamofire
import Swinject
import GoogleMaps

protocol NearbyPlacesModuleBuildable: ModuleBuildable {
    func buildModule(with nearbyPlaces: [NearbyPlace]) -> Beat.Module?
}

class NearbyPlacesBuilder: NearbyPlacesModuleBuildable {
    
    // MARK: - Properties
    
    private let container: Container
    
    // MARK: - Init
    
    init(container: Container) {
        self.container = container
        Container.loggingFunction = nil
    }
    
    // MARK: - ModuleBuildable
    func buildModule(with nearbyPlaces: [NearbyPlace]) -> Beat.Module? {
        bootstrapGoogleMaps()
        registerView()
        registerNetwork()
        registerPresenter(nearbyPlaces: nearbyPlaces)
        
        guard let nearbyPlacesViewController = container.resolve(NearbyPlacesViewable.self) as? UIViewController  else { return nil }
        return Beat.Module(viewController: nearbyPlacesViewController)
    }
    
    private func registerView() {
        container.register(NearbyPlacesViewable.self, factory: { _ in
            NearbyPlacesViewController.instantiate()
        }).initCompleted({ (r, view) in
            if let nearbyPlacesViewController = view as? NearbyPlacesViewController {
                nearbyPlacesViewController.presenter = r.resolve(NearbyPlacesPrentable.self)!
            }
        }).inObjectScope(.container)
    }
    
    func registerNetwork() {
        container.register(SessionManager.self, factory: { r in
            SessionManager()
        }).inObjectScope(.container)
        
        container.register(RequestRetrier.self, factory: { r in
            NetworkRequestRetrier()
        }).inObjectScope(.container)
    }
    
    private func registerPresenter(nearbyPlaces: [NearbyPlace]) {
        container.register(NearbyPlacesPrentable.self, factory: { r in
            NearbyPlacesPresenter(nearbyPlacesView: r.resolve(NearbyPlacesViewable.self)!, nearbyPlaces: nearbyPlaces)
        }).initCompleted({ (r, presenter) in
            if let presenter = presenter as? NearbyPlacesPresenter {
                presenter.delegate = r.resolve(NearbyPlacesRouter.self)!
            }
        }).inObjectScope(.container)
    }
    
    private func bootstrapGoogleMaps() {
        GMSServices.provideAPIKey(Constants.ThirdParty.Google.Maps.apiKey)
    }
}
