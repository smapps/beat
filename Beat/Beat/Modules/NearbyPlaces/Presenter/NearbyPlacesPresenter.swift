//
//  NearbyPlacesPresenter.swift
//  Beat
//
//  Created by Sameh Mabrouk on 26/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import CoreLocation.CLLocation

class NearbyPlacesPresenter: NearbyPlacesPrentable {
    // MARK: - Properties
    
    private weak var nearbyPlacesView: NearbyPlacesViewable?
    private var placesMarkers = [PlaceMarker]()
    
    var nearbyPlaces: [NearbyPlace]
    
    weak var delegate: NearbyPresenterDelegate?
    
    init(nearbyPlacesView: NearbyPlacesViewable, nearbyPlaces: [NearbyPlace]) {
        self.nearbyPlacesView = nearbyPlacesView
        self.nearbyPlaces = nearbyPlaces
    }

    func getPlacesMarkers() -> [PlaceMarker] {
        nearbyPlaces.forEach { place in
            placesMarkers.append(PlaceMarker(place: place))
        }
        
        return placesMarkers
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection() -> Int {
        return nearbyPlaces.count
    }
    
    func itemAtIndex(index: Int, in section: Int) -> PlaceCellViewModel? {
        let place = nearbyPlaces[index]
        let placeCategory: String
        if let category = place.category {
            placeCategory = category
        } else {
            placeCategory = ""
            assertionFailure("[Place \(place.name)] doesn't have a category")
        }
        return PlaceCellViewModel(name: place.name, category: placeCategory, address: place.address, icon: place.icon)
    }
    
    func findNearestPlaceTo(position: CLLocationCoordinate2D) -> PlaceMarker? {
        let location = CLLocation(latitude: position.latitude, longitude: position.longitude)
        var locations = [CLLocation]()
        nearbyPlaces.forEach { place in
            locations.append(CLLocation(latitude: place.position.latitude, longitude: place.position.longitude))
        }
        
        guard let nearestLocation = locations.min(by:{ $0.distance(from: location) < $1.distance(from: location) })  else { return nil }
        
        let nearestPlace = placesMarkers.first { $0.place.position.latitude == nearestLocation.coordinate.latitude && $0.place.position.longitude == nearestLocation.coordinate.longitude }
        return nearestPlace
    }
    
    func backButtonTapped() {
        delegate?.didTappBackButton()
    }
}
