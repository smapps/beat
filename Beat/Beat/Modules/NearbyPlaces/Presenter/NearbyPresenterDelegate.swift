//
//  NearbyPresenterDelegate.swift
//  Beat
//
//  Created by Sameh Mabrouk on 04/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

protocol NearbyPresenterDelegate: class {
    func didTappBackButton()
}
