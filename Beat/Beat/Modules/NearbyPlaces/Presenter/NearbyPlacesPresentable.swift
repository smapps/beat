//
//  NearbyPlacesPresentable.swift
//  Beat
//
//  Created by Sameh Mabrouk on 26/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import CoreLocation.CLLocation

protocol NearbyPlacesPrentable {
    func getPlacesMarkers() -> [PlaceMarker]
    
    func numberOfSections() -> Int
    func numberOfRowsInSection() -> Int
    func itemAtIndex(index: Int, in section: Int) -> PlaceCellViewModel?
    
    func findNearestPlaceTo(position: CLLocationCoordinate2D) -> PlaceMarker?
    
    func backButtonTapped()
}
