//
//  NearbyPlacesRouter.swift
//  Beat
//
//  Created by Sameh Mabrouk on 28/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import UIKit

protocol NearbyPlacesRouterDelegate: class {
    func nearbyPlacesRouterDidFinish(_ nearbyPlacesRouter: NearbyPlacesRouter)
}

class NearbyPlacesRouter: Router {
    // MARK: - Properties
    
    private let nearbyPlacesBuilder: NearbyPlacesModuleBuildable
    
    var rootViewController: NavigationControllable?
    
    var nearbyPlaces: [NearbyPlace]?
    
    weak var delegate: NearbyPlacesRouterDelegate?
    
    
    // MARK: - Init
    
    init(nearbyPlacesBuilder: NearbyPlacesModuleBuildable) {
        self.nearbyPlacesBuilder = nearbyPlacesBuilder
    }
    
    // MARK: - Router
    
    // MARK: - Life cycle
    
    override func start() {
        if let nearbyPlaces = nearbyPlaces, let nearbyPlacesViewController = nearbyPlacesBuilder.buildModule(with: nearbyPlaces)?.viewController, let rootViewController = rootViewController {
            rootViewController.pushViewController(nearbyPlacesViewController, animated: true)
        }
    }
    
    override func stop() {
        rootViewController?.popCurrentViewController(animated: true)
    }
}

extension NearbyPlacesRouter: NearbyPresenterDelegate {
    func didTappBackButton() {
        delegate?.nearbyPlacesRouterDidFinish(self)
    }
}
