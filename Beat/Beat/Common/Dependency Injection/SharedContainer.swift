//
//  SharedContainer.swift
//  Beat
//
//  Created by Sameh Mabrouk on 28/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import Swinject

extension Container {
    static let shared: Container = {
        let c = Container()
        
        c.register(NearbyPlacesModuleBuildable.self, factory: { r in
            NearbyPlacesBuilder(container: c)
        }).inObjectScope(.container)
        
        c.register(SearchPlacesBuilder.self, factory: { r in
            SearchPlacesBuilder(container: c)
        }).inObjectScope(.container)
        
        c.register(NearbyPlacesRouter.self, factory: { r in
            NearbyPlacesRouter(nearbyPlacesBuilder: r.resolve(NearbyPlacesModuleBuildable.self)!)
        }).initCompleted({ (r, router) in
            router.delegate = r.resolve(SearchPlacesRouter.self)!
        }).inObjectScope(.container)
        
        c.register(SearchPlacesRouter.self, factory: { r in
            SearchPlacesRouter(searchPlacesBuilder: r.resolve(SearchPlacesBuilder.self)!)
        }).initCompleted({ (r, router) in
            router.neabyPlacesRouter = r.resolve(NearbyPlacesRouter.self)!
        }).inObjectScope(.container)
                
        return c
    }()
}

