//
//  NavigationControllable.swift
//  Beat
//
//  Created by Sameh Mabrouk on 03/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

import UIKit

@objc protocol NavigationControllable: class {
    func pushViewController(_ viewController: UIViewController, animated: Bool)
    
    @discardableResult
    func popCurrentViewController(animated: Bool) -> UIViewController?
}

extension UINavigationController: NavigationControllable {
    func popCurrentViewController(animated: Bool) -> UIViewController? {
        return self.popViewController(animated: animated)
    }
}
