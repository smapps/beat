//
//  CloseBarButtonActionHandling.swift
//  Beat
//
//  Created by Sameh Mabrouk on 04/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

import UIKit

@objc protocol BackBarButtonActionHandling: class {
    func backButtonTapped()
}

extension BackBarButtonActionHandling where Self: UIViewController {
    
    func addBackBarButtonItem(imageName: String = Image.back) {
        navigationItem.leftBarButtonItem = closeBarButtonItem(imageName: imageName)
    }
    
    func removeCloseBarButtonItem() {
        navigationItem.leftBarButtonItem = nil
    }
    
    private func closeBarButtonItem(imageName: String) -> UIBarButtonItem {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: imageName), for: .normal)
        button.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        
        return UIBarButtonItem(customView: button)
    }
}
