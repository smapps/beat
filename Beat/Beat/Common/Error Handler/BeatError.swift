//
//  BeatError.swift
//  Beat
//
//  Created by Sameh Mabrouk on 02/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

import Foundation

@objc enum BeatError:Int, Error {
    case JSONParsing
    case noResponse
    case noInternetConnection
}

// MARK: - Error Descriptions

extension BeatError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .JSONParsing:
            return NSLocalizedString("Didn't get any data from API", comment: "Parsing Error")
        case .noResponse:
            return NSLocalizedString("No respose from server", comment: "Server Error")
        case .noInternetConnection:
            return NSLocalizedString("The Internet connection appears to be offline.", comment: "no internet connection")
        }
    }
}

// MARK: - Error MetaData

extension BeatError: CustomNSError {
    public static var errorDomain: String {
        return "com.foursquare.error"
    }
    
    public var errorCode: Int {
        switch self {
        case .JSONParsing:
            return 1001
        case .noResponse:
            return 1002
        case .noInternetConnection:
            return -1009
        }
    }
    
    public var errorUserInfo: [String: Any] {
        switch self {
        case .JSONParsing:
            return [NSLocalizedDescriptionKey: "No data from server"]
        case .noResponse:
            return [NSLocalizedDescriptionKey: "No respose from server"]
        case .noInternetConnection:
            return [NSLocalizedDescriptionKey: "No internet connection"]
        }
    }
}

// MARK: - Error Details

extension BeatError {
    func errorDetails() -> ErrorDetails {
        switch self {
        case .JSONParsing:
            return ErrorDetails(errorDescription: BeatError.JSONParsing.errorDescription, errorTitle: BeatError.JSONParsing.errorUserInfo[NSLocalizedDescriptionKey] as? String)
        case .noResponse:
            return ErrorDetails(errorDescription: BeatError.noResponse.errorDescription, errorTitle: BeatError.noResponse.errorUserInfo[NSLocalizedDescriptionKey] as? String)
        case .noInternetConnection:
            return ErrorDetails(errorDescription: BeatError.noInternetConnection.errorDescription, errorTitle: BeatError.noInternetConnection.errorUserInfo[NSLocalizedDescriptionKey] as? String)            
        }
    }
}

@objc public class ErrorDetails: NSObject {
    @objc public let errorDescription: String?
    @objc public let errorTitle: String?
    
    @objc public init(errorDescription: String?, errorTitle: String?) {
        self.errorDescription = errorDescription
        self.errorTitle = errorTitle
    }
}
