//
//  JSONDecoder+Extensions.swift
//  Beat
//
//  Created by Sameh Mabrouk on 02/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

import Foundation
import Alamofire

/// Generic function to handle Alamofire responses into our Codable types until Alamofire support it.
extension JSONDecoder {
    func decodeResponse<T: Decodable>(from response: DataResponse<Data>) -> Result<T> {
        if let error = response.error {
            if error._code == -1009 {
                return .failure(BeatError.noInternetConnection)
            } else {
                return .failure(BeatError.noResponse)
            }
        } else {
            guard let responseData = response.data else {
                return .failure(BeatError.JSONParsing)
            }
            
            do {
                let item = try decode(T.self, from: responseData)
                return .success(item)
            } catch {
                return .failure(BeatError.JSONParsing)
            }
        }
    }
}
