//
//  UIColor+Palette.swift
//  Beat
//
//  Created by Sameh Mabrouk on 30/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import UIKit

extension UIColor {
    static let beatGrey = UIColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1.0)
    static let beatShadowColor = UIColor(red: 202/255, green: 202/255, blue: 202/255, alpha: 1.0)
    static let flatGreen = UIColor(red: 46/255, green: 204/255, blue: 113/255, alpha: 1.0)
    static let flatRed = UIColor(red: 231/255, green: 76/255, blue: 60/255, alpha: 1.0)
    static let flatGray = UIColor(red: 149/255, green: 165/255, blue: 166/255, alpha: 1.0)
    static let binSelectedColor = UIColor(red: 217/255, green: 27/255, blue: 68/255, alpha: 1.0)
    static let binUnselectedColor = UIColor(red: 127/255, green: 185/255, blue: 170/255, alpha: 1.0)
    static let navBarTitleColor = UIColor(red: 217/255, green: 27/255, blue: 68/255, alpha: 1.0)
}
