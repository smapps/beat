//
//  Constants.swift
//  Beat
//
//  Created by Sameh Mabrouk on 27/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import UIKit

struct Constants {

    // MARK: - App
    
    struct App {
        static let baseURL = Config.string(for: .baseURL)
    }
    
    // MARK: - ThirdParty
    
    struct ThirdParty {
        struct Google {
            struct Maps {
                static let apiKey = Config.string(for: .googleMapsAPIKey)
            }
        }
    }
    
    // MARK: - Roller
    
    struct Roller {
        static let defaultCollapsedHeight: CGFloat = 68.0
        static let defaultPartialRevealHeight: CGFloat = 264.0
        static let drawerChangedDistanceFromBottom: CGFloat = 268.0
    }
}
