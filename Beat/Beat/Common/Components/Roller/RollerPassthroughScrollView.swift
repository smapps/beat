//
//  RollerPassthroughScrollView.swift
//  Beat
//
//  Created by Sameh Mabrouk on 29/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import UIKit

protocol RollerPassthroughScrollViewDelegate: class {
    func shouldTouchPassthroughScrollView(scrollView: RollerPassthroughScrollView, point: CGPoint) -> Bool
    func viewToReceiveTouch(scrollView: RollerPassthroughScrollView, point: CGPoint) -> UIView
}

class RollerPassthroughScrollView: UIScrollView {
    
    weak var touchDelegate: RollerPassthroughScrollViewDelegate?
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if let touchDelegate = touchDelegate,
            touchDelegate.shouldTouchPassthroughScrollView(scrollView: self, point: point)
        {
            return touchDelegate.viewToReceiveTouch(scrollView: self, point: point).hitTest(touchDelegate.viewToReceiveTouch(scrollView: self, point: point).convert(point, from: self), with: event)
        }
        
        return super.hitTest(point, with: event)
    }
}
