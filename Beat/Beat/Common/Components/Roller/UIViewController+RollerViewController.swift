//
//  UIViewController+RollerViewController.swift
//  Beat
//
//  Created by Sameh Mabrouk on 29/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import UIKit

extension UIViewController {

    var rollerViewController: RollerViewController? {
        var parentVC = parent
        while parentVC != nil {
            if let rollerViewController = parentVC as? RollerViewController {
                return rollerViewController
            }
            parentVC = parentVC?.parent
        }
        return nil
    }
}
