//
//  ModuleBuildable.swift
//  Beat
//
//  Created by Sameh Mabrouk on 28/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

protocol ModuleBuildable {
    func buildModule() -> Beat.Module?
}

extension ModuleBuildable {
    func buildModule() -> Beat.Module? {
        return nil
    }
}
