//
//  BeatModule.swift
//  Beat
//
//  Created by Sameh Mabrouk on 28/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import UIKit.UIViewController

enum Beat {
    struct Module {
        let viewController: UIViewController
    }
}
