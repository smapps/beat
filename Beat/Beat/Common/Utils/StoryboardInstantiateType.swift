//
//  StoryboardInstantiateType.swift
//  Beat
//
//  Created by Sameh Mabrouk on 27/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

enum StoryboardInstantiateType {
    case initial
    case identifier(String)
}

enum StoryboardName: String {
    case nearbyPlaces = "NearbyPlaces"
}
