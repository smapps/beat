//
//  SceneDelegate.swift
//  Beat
//
//  Created by Sameh Mabrouk on 23/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

import UIKit
import Swinject

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var appRouter: SearchPlacesRouter!

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            window = UIWindow(windowScene: windowScene)
            appRouter = Container.shared.resolve(SearchPlacesRouter.self)
            appRouter.window = window
            appRouter.start()
        }
    }
}

