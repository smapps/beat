//
//  SearchPlacesPresenterTests.swift
//  BeatTests
//
//  Created by Sameh Mabrouk on 05/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

@testable import Beat

import XCTest
import CoreLocation.CLLocation

class SearchPlacesPresenterTests: XCTestCase {
    // MARK: - Test Properties
    
    private let searchPlacesInteractorMock = SearchPlacesInteractorMock()
    private let searchPlacesPresenterDelegateSpy = SearchPlacesPresenterDelegateSpy()
    private let searchPlacesViewControllerMock = SearchPlacesViewControllerMock()
    private let nearbyPlacesMock = [NearbyPlace(name: "Concerto Koffie",
                                                category: "cafe", address: "Utrechtsestraat 52, 1017 VP Amsterdam",
                                                icon: "https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png",
                                                position: CLLocationCoordinate2DMake(52.36344889999999, 4.898373800000001)),
                                    NearbyPlace(name: "Café Blond",
                                                category: "cafe",
                                                address: "Ferdinand Bolstraat 44H, 1072 LL Amsterdam",
                                                icon: "https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png",
                                                position: CLLocationCoordinate2DMake(52.3560132, 4.8903867))
    ]
    private var sut: SearchPlacesPresenter!
    
    // MARK: - Test Life cycle
    
    override func setUp() {
        super.setUp()
        
        sut = SearchPlacesPresenter(interactor: searchPlacesInteractorMock, view: searchPlacesViewControllerMock)
    }
    
    override func tearDown() {
        sut = nil
        
        super.tearDown()
    }
    
    // MARK: - Tests
    
    func testGetNearbyPlacesAndInvokeViewUpdateWhenNewPlacesArrive() {
        // Given
        searchPlacesInteractorMock.stubbedDeterminUserLocationCompletion = (UserLocation(lat: 52.364539, lng: 4.899759))
        searchPlacesInteractorMock.stubbedGetNearbyPlacesCompletion  = (nearbyPlacesMock, nil)
        
        // When
        sut.viewDidLoad()
        sut.getNearbyPlaces(forQuery: "Cafe")
        
        // Then
        XCTAssertTrue(searchPlacesViewControllerMock.invokedUpdate)
    }
    
    func testGetNeearbyPlacesAndInvokeErrorWhenNoPlacesArrive() {
        // Given
        searchPlacesInteractorMock.stubbedDeterminUserLocationCompletion = (UserLocation(lat: 52.364539, lng: 4.899759))
        searchPlacesInteractorMock.stubbedGetNearbyPlacesCompletion  = (nil, BeatError.JSONParsing.errorDetails())
        
        // When
        sut.viewDidLoad()
        sut.getNearbyPlaces(forQuery: "Cafe")
        
        // Then
        XCTAssertTrue(searchPlacesViewControllerMock.invokedShowError)
    }
    
    func testGetNearbyPlacesAndInvokeErrorWhenThereIsNoUserLocation() {
        // Given
        searchPlacesInteractorMock.stubbedGetNearbyPlacesCompletion  = (nearbyPlacesMock, nil)

        // When
         sut.viewDidLoad()
         sut.getNearbyPlaces(forQuery: "Cafe")
         
         // Then
         XCTAssertTrue(searchPlacesViewControllerMock.invokedShowError)
    }
    
    func testDidObtainNearbyPlaces() {
        // Given
        sut.delegate = searchPlacesPresenterDelegateSpy
        
        // When
        sut.didObtainNearbyPlaces()
        
        // Then
        XCTAssertTrue(searchPlacesPresenterDelegateSpy.invokeDidObtainNearbyPlaces)
    }
}
