//
//  PlacesServiceTests.swift
//  BeatTests
//
//  Created by Sameh Mabrouk on 05/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

@testable import Beat

import XCTest

class PlacesServiceTests: XCTestCase {
    // MARK: - Test Properties

    private let sessionManagerMock = SessionManagerMock()
    private var sut: PlacesService!
    
    // MARK: - Test Life cycle
    override func setUp() {
        super.setUp()
        
        sut = PlacesService(sessionManager: sessionManagerMock, requestRetrier: NetworkRequestRetrier())
    }
    
    override func tearDown() {
        sut = nil
        
        super.tearDown()
    }
    
    // MARK: - Tests
    
    func testFetchNearbyPlaces() {
        // When
        sut.fetchNearbyPlaces(query: "Coffe", location: "52.364539,4.899759") { (nearbyPlaces, error) in
            // Then
            XCTAssertEqual(nearbyPlaces?.count, 20)
        }
    }
}
