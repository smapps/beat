//
//  SearchPlacesInteractorTests.swift
//  BeatTests
//
//  Created by Sameh Mabrouk on 05/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

@testable import Beat

import XCTest
import CoreLocation.CLLocation

class SearchPlacesInteractorTests: XCTestCase {
    // MARK: - Test Properties

    private let placesServiceMock = PlacesServiceMock()
    private let locationServiceMock = LocationServiceMock()
    private var sut: SearchPlacesInteractor!
    private let nearbyPlacesMock = [NearbyPlace(name: "Concerto Koffie",
                                                category: "cafe", address: "Utrechtsestraat 52, 1017 VP Amsterdam",
                                                icon: "https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png",
                                                position: CLLocationCoordinate2DMake(52.36344889999999, 4.898373800000001)),
                                    NearbyPlace(name: "Café Blond",
                                                category: "cafe",
                                                address: "Ferdinand Bolstraat 44H, 1072 LL Amsterdam",
                                                icon: "https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png",
                                                position: CLLocationCoordinate2DMake(52.3560132, 4.8903867))
]
    // MARK: - Test Life cycle

    override func setUp() {
        super.setUp()
        
        sut = SearchPlacesInteractor(placesService: placesServiceMock, locationService: locationServiceMock)
    }
    
    override func tearDown() {
        sut = nil
        
        super.tearDown()
    }
    
    // Tests
    
    func testDetermineUserLocationWhenLocationServiceIsEnabled() {
        // Given
        locationServiceMock.isLocationServiceEnabled = true
        
        // When
        sut.determineUserLocation { location in
            // Then
            XCTAssertNotNil(location)
            XCTAssertEqual(location?.lat, 52.36795609763071)
            XCTAssertEqual(location?.lng, 4.895555168247901)
        }
    }
    
    func testDetermineUserLocationWhenLocationServiceIsDisabled() {
        // Given
        locationServiceMock.isLocationServiceEnabled = false
        
        // When
        sut.determineUserLocation { location in
            // Then
            XCTAssertEqual(location?.lng, 0.0)
            XCTAssertEqual(location?.lat, 0.0)
        }
    }
    
     func testGetNearbyPlacesAndInvokeViewUpdateWhenNewPlacesArrive() {
        // Given
        placesServiceMock.stubbedFetchNearbyPlacesCompletionResult = (nearbyPlacesMock, nil)
        
        // When
        sut.getNearbyPlaces(forQuery: "Cafe", aroundLocation: "52.364539,4.899759") { (nearbyPlaces, error) in
            // Then
            XCTAssertEqual(nearbyPlaces?.count, 2)
        }
    }
    
    func testGetNeearbyPlacesAndInvokeErrorWhenNoPlacesArrive() {
        // Given
        placesServiceMock.stubbedFetchNearbyPlacesCompletionResult = (nil, BeatError.JSONParsing.errorDetails())
        
        // When
        sut.getNearbyPlaces(forQuery: "Cafe", aroundLocation: "52.364539,4.899759") { (nearbyPlaces, error) in
            // Then
            XCTAssertEqual(error?.errorDescription, "Didn't get any data from API")
        }
    }
}
