//
//  NearbyPlacesPresenter.swift
//  BeatTests
//
//  Created by Sameh Mabrouk on 24/12/2019.
//  Copyright © 2019 Sameh Mabrouk. All rights reserved.
//

@testable import Beat

import XCTest
import MapKit

class NearbyPlacesPresenterTests: XCTestCase {
    
    // MARK: - Test Properties
    
    private let nearbyViewControllerMock = NearbyViewControllerMock()
    private let nearbyPlacesMock = [NearbyPlace(name: "Concerto Koffie",
                                                category: "cafe", address: "Utrechtsestraat 52, 1017 VP Amsterdam",
                                                icon: "https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png",
                                                position: CLLocationCoordinate2DMake(52.36344889999999, 4.898373800000001)),
                                    NearbyPlace(name: "Café Blond",
                                                category: "cafe",
                                                address: "Ferdinand Bolstraat 44H, 1072 LL Amsterdam",
                                                icon: "https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png",
                                                position: CLLocationCoordinate2DMake(52.3560132, 4.8903867))
    ]
    private var sut: NearbyPlacesPresenter!
    
    // MARK: - Test Life cycle
    
    override func setUp() {
        super.setUp()
        
        sut = NearbyPlacesPresenter(nearbyPlacesView: nearbyViewControllerMock, nearbyPlaces: nearbyPlacesMock)
    }
    
    override func tearDown() {
        sut = nil
        
        super.tearDown()
    }
    
    // MARK: - Tests

    func testGetPlacesMarkers() {
        // When
        let placesMarkers = sut.getPlacesMarkers()
        
        // Then
        XCTAssertEqual(placesMarkers.count, 2)
    }
    
    func testNumberOfSections() {
        // When
        let numberOfSections = sut.numberOfSections()
        
        // Then
        XCTAssertEqual(numberOfSections, 1)
    }
    
    func testNumberOfRowsInSection() {
        // When
        let numberOfItemsInSection = sut.numberOfRowsInSection()
        
        // Then
        XCTAssertEqual(numberOfItemsInSection, 2)
    }
    
    func testItemAtIndex() {
        // When
        let item = sut.itemAtIndex(index: 1, in: 0)
        
        // Then
        XCTAssertEqual(item?.name, "Café Blond")
    }
}
