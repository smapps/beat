//
//  LocationServiceMock.swift
//  BeatTests
//
//  Created by Sameh Mabrouk on 05/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

@testable import Beat

class LocationServiceMock: LocationServiceChecking {
    var isLocationServiceEnabled = false
    var auth: Auth!
    
    enum Auth {
        case authorized
        case denied
    }
    
    func requestAuthorization() {
        if isLocationServiceEnabled {
            auth = .authorized
        } else {
            auth = .denied
        }
    }
    
    func requestUserLocation(completion: @escaping UserLocationBlock) {
        switch auth {
        case .authorized?:
            completion(UserLocation(lat: 52.36795609763071, lng: 4.895555168247901))
        default:
            completion(UserLocation(lat: 0.0, lng: 0.0))
            break
        }
    }
}
