//
//  PlacesServiceMock.swift
//  BeatTests
//
//  Created by Sameh Mabrouk on 05/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

@testable import Beat

class PlacesServiceMock: PlacesFetching {
    var invokedFetchNearbyPlaces = false
    var invokedFetchNearbyPlacesCount = 0
    var invokedFetchNearbyPlacesParameters: (query: String, location: String)?
    var invokedFetchNearbyPlacesParametersList = [(query: String, location: String)]()
    var stubbedFetchNearbyPlacesCompletionResult: ([NearbyPlace]?, ErrorDetails?)?
    
    func fetchNearbyPlaces(query: String, location: String, completion: @escaping ([NearbyPlace]?, ErrorDetails?) -> Void) {
        invokedFetchNearbyPlaces = true
        invokedFetchNearbyPlacesCount += 1
        invokedFetchNearbyPlacesParameters = (query, location)
        invokedFetchNearbyPlacesParametersList.append((query, location))
        if let result = stubbedFetchNearbyPlacesCompletionResult {
            completion(result.0, result.1)
        }
    }
}
