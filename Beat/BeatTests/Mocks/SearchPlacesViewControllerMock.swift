//
//  SearchPlacesViewControllerMock.swift
//  BeatTests
//
//  Created by Sameh Mabrouk on 05/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

@testable import Beat

class SearchPlacesViewControllerMock: NSObject, SearchPlacesViewable {
    var invokedUpdate = false
    var invokedUpdateCount = 0
    var invokedShowError = false
    var invokedShowErrorCount = 0
    
    func update() {
        invokedUpdate  = true
        invokedUpdateCount += 1
    }
    
    func showError(_ error: ErrorDetails!) {
        invokedShowError = true
        invokedShowErrorCount += 1
    }
}
