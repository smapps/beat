//
//  NearbyViewControllerMock.swift
//  BeatTests
//
//  Created by Sameh Mabrouk on 03/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

@testable import Beat

class NearbyViewControllerMock: UIViewController, NearbyPlacesViewable {
    var invokedUpdate = false
    var invokedUpdateCount = 0
    func update() {
        invokedUpdate = true
        invokedUpdateCount += 1
    }
    var invokedShowError = false
    var invokedShowErrorCount = 0
    var invokedShowErrorParameters: (error: Error, Void)?
    var invokedShowErrorParametersList = [(error: Error, Void)]()
    func showError(error: Error) {
        invokedShowError = true
        invokedShowErrorCount += 1
        invokedShowErrorParameters = (error, ())
        invokedShowErrorParametersList.append((error, ()))
    }
}
