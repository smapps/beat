//
//  SearchPlacesPresenterDelegateSpy.swift
//  BeatTests
//
//  Created by Sameh Mabrouk on 05/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

@testable import Beat

class SearchPlacesPresenterDelegateSpy: NSObject, SearchPlacesPresenterDelegate {
    var invokeDidObtainNearbyPlaces = false
    var invokeDidObtainNearbyPlacesCount = 0
    
    func didObtain(_ nearbyPlaces: [NearbyPlace]!) {
        invokeDidObtainNearbyPlaces = true
        invokeDidObtainNearbyPlacesCount += 1
    }
}
