//
//  SearchPlacesInteractorMock.swift
//  BeatTests
//
//  Created by Sameh Mabrouk on 05/01/2020.
//  Copyright © 2020 Sameh Mabrouk. All rights reserved.
//

@testable import Beat

class SearchPlacesInteractorMock: NSObject, SearchPlacesInteractable {
    var invokedDetermineUserLocation = false
    var invokedGetNearbyPlaces = false
    var stubbedDeterminUserLocationCompletion: ((UserLocation?))?
    var stubbedGetNearbyPlacesCompletion: ([NearbyPlace]?, ErrorDetails?)?

    func determineUserLocation(completionHandler completion: ((UserLocation?) -> Void)) {
        invokedDetermineUserLocation = true
        if let result = stubbedDeterminUserLocationCompletion {
            completion(result)
        }
    }
    
    func getNearbyPlaces(forQuery query: String?, aroundLocation location: String?, withCompletionHandler completion: @escaping ([NearbyPlace]?, ErrorDetails?) -> Void) {
        invokedGetNearbyPlaces = true
        if let result = stubbedGetNearbyPlacesCompletion {
            completion(result.0, result.1)
        }
    }
}
